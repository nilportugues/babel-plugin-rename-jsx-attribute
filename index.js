module.exports = function() {
  return {
    name: 'remove-jsx-attribute',
    visitor: {
      JSXAttribute(path) {
        const transformTarget = this.opts.attributes[path.node.name.name];
        if (transformTarget === null) {
          path.remove();
        }
        if (transformTarget) {
          path.node.name.name = transformTarget;
        }
      }
    }
  }
};
