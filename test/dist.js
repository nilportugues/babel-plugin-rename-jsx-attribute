import React from 'react';

export default function () {
  return React.createElement(
    "h1",
    { bar: "foo", bar2: "foo2" },
    "Hello World"
  );
}
