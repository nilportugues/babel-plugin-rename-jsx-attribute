# babel-plugin-rename-jsx-attribute

Renames the react element attribute in jsx

```
npm install babel-plugin-rename-jsx-attribute --save-dev
```

can use like:
```
{
  "presets": ["react"],
  "plugins": [
    [
      "rename-jsx-attribute",
      {
        "attributes": {
          "foo": "bar",
          "foo2": "bar2"
          "foo3": null
        }
      }
    ]
  ]
}
```
